# Bible Skel Checker

Bible Skel Checker was a volunteer job and is an app created using HTML/Bootstrap/AngularJS.

It was designed to parse Bible books in .usfm format which are written/translated by people and check their strucuture (number of chapters/verses) against "template" books.

The template books must be preloaded in the app and you can upload your custom books into the server to compare.
