(function() {
    angular.module('BibleSkelChecker', ['ui.bootstrap', 'ngAnimate', 'ngTouch', 'angularFileUpload'])
		.controller('MainCtrl', ['usfmReader', 'FileUploader', function(usfmReader, FileUploader) {
			var self = this;
				
			self.books = ["Genesis", "Exodus", "Leviticus", "Numbers", "Deuteronomy", "Joshua", "Judges", "Ruth", "1 Samuel", "2 Samuel", "1 Kings", "2 Kings", "1 Chronicles", "2 Chronicles", "Ezra", "Nehemiah", "Esther", "Job", "Psalms", "Proverbs", "Ecclesiastes", "Song of Solomon", "Isaiah", "Jeremiah", "Lamentations", "Ezekiel", "Daniel", "Hosea", "Joel", "Amos", "Obadiah", "Jonah", "Micah", "Nahum", "Habakkuk", "Zephaniah", "Haggai", "Zechariah", "Malachi", "Matthew", "Mark", "Luke", "John", "Acts of the Apostles", "Romans", "1 Corinthians", "2 Corinthians", "Galatians", "Ephesians", "Philippians", "Colossians", "1 Thessalonians", "2 Thessalonians", "1 Timothy", "2 Timothy", "Titus", "Philemon", "Hebrews", "James", "1 Peter", "2 Peter", "1 John", "2 John", "3 John", "Jude", "Revelation"];
			
			self.comparisonReady = false;
			self.templateBook = {};
			self.customBook = {};

			self.uploader = new FileUploader({url: './upload.php', removeAfterUpload: true, autoUpload: true});

			self.uploader.filters.push({
				name: 'bookFilter',
				fn: function(item, options) {
					var type = '|' + item.name.slice(item.name.lastIndexOf('.') + 1) + '|';
					console.log(type);
					return '|usfm|'.indexOf(type) !== -1;
				}
			});
			
			self.uploader.onSuccessItem = function(fileItem, response, status, headers) {
				self.chooseBook(fileItem.file.name.split('.')[0],self.customBook, true)
			};
			
			self.groupComparison = function() {
				self.compare = {template: self.templateBook, custom: self.customBook};
				self.comparisonReady = true;
			};

			self.reset = function() {
				self.uploader.clearQueue();
				angular.element(document.querySelector('#customBible')).val('');
				self.comparisonReady = false;
				self.templateBook = {};
				self.customBook = {};
				self.compare = {};
			};
			
			self.countChapters = function(book) {
				return book.split('\\c').length - 1;
			};

			self.countVerses = function (chapter) {
				return chapter.text.split('\\v').length - 1;
			};
			
			self.parseBook = function(tgtBook) {
				try {
					for (i = 0; i < self.countChapters(tgtBook.book); i++) { 
						tgtBook.chapters.push ({
							id: i + 1,
							text: tgtBook.book.split('\\c')[i + 1],
							versesCount: ""
						});
						
						tgtBook.chapters[i].versesCount = self.countVerses(tgtBook.chapters[i]);
					};
					tgtBook.isValid = true;
					tgtBook.error = null;
					tgtBook.success = 'Book "' + tgtBook.bookName + '" succesfuly loaded!';
				} catch(err) {
					tgtBook.isValid = false;
					tgtBook.success = null;
					tgtBook.error = 'Error parsing "' + tgtBook.bookName + '" book! (' + err.status + ')';
				}
			};
			
			self.chooseBook = function(book, target, custom){
				target.bookName = book;
				
				usfmReader.readBook(target.bookName, custom)
				 .then( function(response) {
					target.book = response.data;
					target.chapters = [];
					self.parseBook(target);
				 }, function(err) {
						target.isValid = false;
						target.success = null;
						target.error = 'Unable to read \"' + target.bookName + '\" book! (Error code: ' + err.status + ')';
				 });
			};
			
			self.changeAccordColor = function (template, custom) {
				if (template === custom) {
					return 'panel-success';
				}
				else {
					return 'panel-danger';
				}
			};
		}])
		
		.factory('usfmReader', ['$http', function($http) {
			return {
				readBook: function(bookName, isCustom) {
					if (!isCustom) {
						return $http.get('./data/' + bookName + '.usfm');
					}
					else {
						return $http.get('./tmp/' + bookName + '.usfm');
					};
				}
            };
		}]);
})();